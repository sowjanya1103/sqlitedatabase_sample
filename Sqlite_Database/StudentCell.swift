//
//  StudentCell.swift
//  Sqlite_Database
//
//  Created by KVANA09 on 14/07/16.
//  Copyright © 2016 KVANA09. All rights reserved.
//

import UIKit

class StudentCell: UITableViewCell {
    
    
    @IBOutlet weak var contentLable: UILabel!

    @IBOutlet weak var edit: UIButton!
    
    
    @IBOutlet weak var delete: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
