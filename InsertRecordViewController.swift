//
//  InsertRecordViewController.swift
//  Sqlite_Database
//
//  Created by KVANA09 on 14/07/16.
//  Copyright © 2016 KVANA09. All rights reserved.
//

import UIKit

class InsertRecordViewController: UIViewController {
    
    
    var isEdit : Bool = false
    var studentData : StudentInfo!
    
    @IBOutlet weak var nameTF: UITextField!
    

    @IBOutlet weak var marksTF: UITextField!
    
    
    
    @IBAction func saveBtnClicked(sender: AnyObject) {
        if(nameTF.text == "")
        {
            Utility.invokeAlertMethod("", strBody: "Please enter student name.", delegate: nil)
        }
        else if(marksTF.text == "")
        {
            Utility.invokeAlertMethod("", strBody: "Please enter student marks.", delegate: nil)
        }
        else
        {
            if(isEdit)
            {
                let studentInfo: StudentInfo = StudentInfo()
                studentInfo.RollNo = studentData.RollNo
                studentInfo.Name = nameTF.text!
                studentInfo.Marks = marksTF.text!
                let isUpdated = ModalManager.getInstance().updateStudentData(studentInfo)
                if isUpdated {
                    Utility.invokeAlertMethod("", strBody: "Record updated successfully.", delegate: nil)
                } else {
                    Utility.invokeAlertMethod("", strBody: "Error in updating record.", delegate: nil)
                }
            }
            else
            {
                let studentInfo: StudentInfo = StudentInfo()
                studentInfo.Name = nameTF.text!
                studentInfo.Marks = marksTF.text!
                let isInserted = ModalManager.getInstance().addStudentData(studentInfo)
                if isInserted {
                    Utility.invokeAlertMethod("", strBody: "Record Inserted successfully.", delegate: nil)
                } else {
                    Utility.invokeAlertMethod("", strBody: "Error in inserting record.", delegate: nil)
                }
            }
            self.navigationController?.popViewControllerAnimated(true)
        }

        
        
        
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
