//
//  HomeScreenViewController.swift
//  Sqlite_Database
//
//  Created by KVANA09 on 14/07/16.
//  Copyright © 2016 KVANA09. All rights reserved.
//

import UIKit

class HomeScreenViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var marrStudentData : NSMutableArray!

 
    @IBAction func deleteBtnClicked(sender: AnyObject) {
        
        
        let btnDelete : UIButton = sender as! UIButton
        let selectedIndex : Int = btnDelete.tag
        let studentInfo: StudentInfo = marrStudentData.objectAtIndex(selectedIndex) as! StudentInfo
        let isDeleted = ModalManager.getInstance().deleteStudentData(studentInfo)
        if isDeleted {
            Utility.invokeAlertMethod("", strBody: "Record deleted successfully.", delegate: nil)
        } else {
            Utility.invokeAlertMethod("", strBody: "Error in deleting record.", delegate: nil)
        }
        self.getStudentData()
    }
 
    @IBAction func editBtnClicked(sender: AnyObject) {
        self.performSegueWithIdentifier("edit", sender: sender)

    }
 
 
    @IBOutlet weak var studentDataTable: UITableView!
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "edit")
        {
            let btnEdit : UIButton = sender as! UIButton
            let selectedIndex : Int = btnEdit.tag
            let viewController : InsertRecordViewController = segue.destinationViewController as! InsertRecordViewController
            viewController.isEdit = true
            viewController.studentData = marrStudentData.objectAtIndex(selectedIndex) as! StudentInfo
        }
    }

    

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        self.getStudentData()
    }


    func getStudentData()
    {
        marrStudentData = NSMutableArray()
        marrStudentData = ModalManager.getInstance().getAllStudentData()
        studentDataTable.reloadData()
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return marrStudentData.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:StudentCell = tableView.dequeueReusableCellWithIdentifier("studentCell") as! StudentCell
        let student:StudentInfo = marrStudentData.objectAtIndex(indexPath.row) as! StudentInfo
        cell.contentLable.text = "Name : \(student.Name)  \n  Marks : \(student.Marks)"
        
        self.studentDataTable.rowHeight = 100
        cell.delete.tag = indexPath.row
        cell.edit.tag = indexPath.row
        return cell
    }
    
}
